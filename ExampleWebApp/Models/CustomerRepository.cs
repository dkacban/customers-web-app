﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleWebApp.Models
{
    public interface ICustomerRepository
    {
        List<Customer> GetAll();
    }

    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAll()
        {
            var customers = new List<Customer>
            {
                new Customer { Name = "Marek", SurName = "Oniszczuk", PhoneNumber = "666666666" },
                new Customer { Name = "Jarek", SurName = "Niszczuk", PhoneNumber = "8888888888" },
                new Customer { Name = "Darek", SurName = "Iszczuk", PhoneNumber = "67666666"}
            };

            return customers;
        }
    }
}
