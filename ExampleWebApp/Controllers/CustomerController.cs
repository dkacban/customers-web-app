﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExampleWebApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ExampleWebApp.Controllers
{
    public class CustomerController : Controller
    {
        private ICustomerRepository _repository;

        public CustomerController(ICustomerRepository repository)
        {
            _repository = repository;
        }

        public ActionResult Index()
        {
            var customers = _repository.GetAll();
            return View(customers);
        }

        public ActionResult List()
        {
            var customers = _repository.GetAll();
            return View(customers);
        }
    }
}